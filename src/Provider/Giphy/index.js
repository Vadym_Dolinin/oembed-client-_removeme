module.exports.Giphy = {
  provider: 'Giphy',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
