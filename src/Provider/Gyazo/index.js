module.exports.Gyazo = {
  provider: 'Gyazo',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
