const expect = require('chai').expect;

const detect = require('./detect');

describe('Gyazo', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://gyazo.com/"`, () => {
    expect(detect('https://gyazo.com/')).to.be.false;
  });

  it(`Should detect "https://gyazo.com/71e107d77e8495f0e54d2e5b6dc5d326"`, () => {
    expect(detect('https://gyazo.com/71e107d77e8495f0e54d2e5b6dc5d326')).to.be.true;
  });
});
