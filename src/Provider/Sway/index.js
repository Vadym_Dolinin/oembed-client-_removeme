module.exports.Sway = {
  provider: 'Sway',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
