module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(www\.)?instagram\.com\/p\/.+/i)
    ||
    url.match(/http(s)?:\/\/instagr\.am\/p\/.+/i)
  )
}
