module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(new RegExp('https://www\.facebook\.com/.+/posts/.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/.+/activity/.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/photo\.php\\?fbid=.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/photos/.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/permalink\.php\\?story_fbid=.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/media/set\\?set=.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/questions/.+', 'i'))
    ||
    url.match(new RegExp('https://www\.facebook\.com/notes/.+/.+/.+', 'i'))
  );

}
