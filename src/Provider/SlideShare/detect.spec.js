const expect = require('chai').expect;

const detect = require('./detect');

describe('SlideShare', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://www.slideshare.net/"`, () => {
    expect(detect('http://www.slideshare.net/')).to.be.false;
  });

  it(`Should NOT detect "https://www.slideshare.net/haraldf/"`, () => {
    expect(detect('https://www.slideshare.net/haraldf/')).to.be.false;
  });

  it(`Should NOT detect "https://www.slideshare.net/mobile/haraldf/"`, () => {
    expect(detect('https://www.slideshare.net/mobile/haraldf/')).to.be.false;
  });

  it(`Should detect "https://www.slideshare.net/haraldf/business-quotes-for-2011"`, () => {
    expect(detect('https://www.slideshare.net/haraldf/business-quotes-for-2011')).to.be.true;
  });

  it(`Should detect "https://www.slideshare.net/mobile/haraldf/business-quotes-for-2011"`, () => {
    expect(detect('https://www.slideshare.net/mobile/haraldf/business-quotes-for-2011')).to.be.true;
  });
});
