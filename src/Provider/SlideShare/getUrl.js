module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://www.slideshare.net/api/oembed/2?format=json&url=${escape(url)}`;
}
