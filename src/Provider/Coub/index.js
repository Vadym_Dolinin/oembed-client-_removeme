module.exports.Coub = {
  provider: 'Coub',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
