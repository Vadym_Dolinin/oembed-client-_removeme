module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!url.match(/http(s)?:\/\/(www\.)?coub\.com\/(view|embed)\/.+/i);
}
