const expect = require('chai').expect;

const detect = require('./detect');

describe('Twitch', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://sway.com/"`, () => {
    expect(detect('https://sway.com/')).to.be.false;
  });

  it(`Should detect "https://sway.com/nLa7rrYhdCmzRyQd"`, () => {
    expect(detect('https://sway.com/nLa7rrYhdCmzRyQd')).to.be.true;
  });
});
