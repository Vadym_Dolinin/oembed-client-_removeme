module.exports.Twitch = {
  provider: 'Twitch',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
