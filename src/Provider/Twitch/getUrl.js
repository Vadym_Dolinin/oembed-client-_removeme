module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://api.twitch.tv/v4/oembed?url=${escape(url)}`;
}
