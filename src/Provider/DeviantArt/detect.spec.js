const expect = require('chai').expect;

const detect = require('./detect');

describe(`DeviantArt`, function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://www.deviantart.com/"`, () => {
    expect(detect('https://www.deviantart.com/')).to.be.false;
  });

  it(`Should detect "https://www.deviantart.com/art/Freak-728597327"`, () => {
    expect(detect('https://www.deviantart.com/art/Freak-728597327')).to.be.true;
  });

  it(`Should detect "http://fav.me/728597327"`, () => {
    expect(detect('http://fav.me/728597327')).to.be.true;
  });

  it(`Should detect "https://wlop.deviantart.com/art/Freak-728597327"`, () => {
    expect(detect('https://wlop.deviantart.com/art/Freak-728597327')).to.be.true;
  });
});
