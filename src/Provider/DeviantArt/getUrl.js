module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://backend.deviantart.com/oembed?format=json&url=${escape(url)}`
}
