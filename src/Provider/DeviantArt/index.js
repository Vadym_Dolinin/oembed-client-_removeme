module.exports.DeviantArt = {
  provider: 'DeviantArt',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
