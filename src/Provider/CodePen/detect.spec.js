const expect = require('chai').expect;

const detect = require('./detect');

describe('CodePen', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "http://codepen.io/"`, () => {
    expect(detect('http://codepen.io/')).to.be.false;
  });

  it(`Should NOT detect "http://codepen.io/pen/wjzyH"`, () => {
    expect(detect('http://codepen.io/pen/wjzyH')).to.be.false;
  });

  it(`Should NOT detect "http://codepen.io/FWeinb/pen/"`, () => {
    expect(detect('http://codepen.io/FWeinb/pen/')).to.be.false;
  });

  it(`Should detect "http://codepen.io/FWeinb/pen/wjzyH"`, () => {
    expect(detect('http://codepen.io/FWeinb/pen/wjzyH')).to.be.true;
  });
});
