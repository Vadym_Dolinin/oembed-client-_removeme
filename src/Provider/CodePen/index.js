module.exports.CodePen = {
  provider: 'CodePen',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
