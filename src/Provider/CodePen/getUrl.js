module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://codepen.io/api/oembed?url=${escape(url)}&format=json`
}
