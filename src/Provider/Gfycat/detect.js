module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s)?:\/\/(www\.)?gfycat\.com\/.{2}\/gifs\/detail\/.+/i)
    ||
    url.match(/http(s)?:\/\/(www\.)?gfycat\.com\/(?!@)[A-z0-9]+(?!\/)$/i)
    );
}
