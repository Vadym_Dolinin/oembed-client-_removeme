module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://api.gfycat.com/v1/oembed?url=${escape(url)}`;
}
