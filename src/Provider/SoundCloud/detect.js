module.exports = (url) => {
  if (typeof url !== 'string') {
    return false;
  }

  return !!(
    url.match(/http(s):\/\/soundcloud\.com\/groups\/.+/i)
    ||
    url.match(/http(s)?:\/\/soundcloud\.com\/.+\/(?!(sets\/)).+/i)
    ||
    url.match(/http(s)?:\/\/soundcloud\.com\/.+\/sets\/.+/i)
    ||
    url.match(/http:\/\/snd\.sc\/.+/i)
  )
}
