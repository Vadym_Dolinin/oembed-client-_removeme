module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://soundcloud.com/oembed?format=json&url=${escape(url)}`;
}
