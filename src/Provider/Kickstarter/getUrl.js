module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://www.kickstarter.com/services/oembed?url=${escape(url)}`;
}
