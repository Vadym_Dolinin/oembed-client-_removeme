const { CodePen } = require('./CodePen');
const { Coub } = require('./Coub');
const { Dailymotion } = require('./Dailymotion');
const { DeviantArt } = require('./DeviantArt');
// const {} = require('./Facebook');
const { Flickr } = require('./Flickr');
const { Gfycat } = require('./Gfycat');
const { Giphy } = require('./Giphy');
const { Gyazo } = require('./Gyazo');
const { Hulu } = require('./Hulu');
const { Instagram } = require('./Instagram');
const { Kickstarter } = require('./Kickstarter');
const { SlideShare } = require('./SlideShare');
const { SoundCloud } = require('./SoundCloud');
const { Sway } = require('./Sway');
const { Twitch } = require('./Twitch');
const { Vimeo } = require('./Vimeo');
const { YouTube } = require('./YouTube');

module.exports.Providers = [
  CodePen,
  Coub,
  Dailymotion,
  DeviantArt,
  Flickr,
  Gfycat,
  Giphy,
  Gyazo,
  Hulu,
  Instagram,
  Kickstarter,
  SlideShare,
  SoundCloud,
  Sway,
  Twitch,
  Vimeo,
  YouTube
];
