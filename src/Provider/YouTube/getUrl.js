module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://www.youtube.com/oembed?format=json&url=${escape(url)}`;
}
