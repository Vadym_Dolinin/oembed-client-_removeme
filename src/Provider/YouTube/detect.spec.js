const expect = require('chai').expect;

const detect = require('./detect');

describe('YouTube', function() {
  it(`Should work without passing URL`, () => {
    expect(detect()).to.be.false;
  });

  it(`Should NOT detect "https://test.com/?url=https://youtube.com/"`, () => {
    expect(detect('https://test.com/?url=https://youtube.com/')).to.be.false;
  });

  it(`Should NOT detect "https://youtube.com/"`, () => {
    expect(detect('https://youtube.com/')).to.be.false;
  });

  it(`Should detect "https://www.youtube.com/watch?v=GurkREc-q4I"`, () => {
    expect(detect('https://www.youtube.com/watch?v=GurkREc-q4I')).to.be.true;
  });

  it(`Should detect "https://www.youtube.com/watch?v=DMjIYp-FwQA&list=PLx7Sys_jBRm-qZJILCLZ94rP6s3GRr1p3"`, () => {
    expect(detect('https://www.youtube.com/watch?video=DMjIYp-FwQA&list=PLx7Sys_jBRm-qZJILCLZ94rP6s3GRr1p3')).to.be.true;
  });

  it(`Should detect "http://youtu.be/GurkREc-q4I"`, () => {
    expect(detect('https://youtu.be/GurkREc-q4I')).to.be.true;
  });

  it(`Should detect "https://youtu.be/GurkREc-q4I"`, () => {
    expect(detect('https://youtu.be/GurkREc-q4I')).to.be.true;
  });
});
