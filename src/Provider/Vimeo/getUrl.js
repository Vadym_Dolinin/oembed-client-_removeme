module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://vimeo.com/api/oembed.json?url=${escape(url)}`;
}
