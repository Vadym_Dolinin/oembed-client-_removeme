module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }
  return `https://www.flickr.com/services/oembed?format=json&url=${escape(url)}`;
}
