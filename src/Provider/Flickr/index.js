module.exports.Flickr = {
  provider: 'Flickr',
  detect: require('./detect'),
  getUrl: require('./getUrl')
}
