const { Providers } = require('./Provider');

module.exports = (url) => {
  if (typeof url !== 'string') {
    return null;
  }

  for (const provider of Providers) {
    if (provider.detect(url) === true) {
      return {
        url,
        provider: provider.provider,
        oEmbedUrl: provider.getUrl(url)
      }
    }
  }

  return null;
};
