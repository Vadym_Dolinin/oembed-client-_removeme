const expect = require('chai').expect;

const detect = require('./');

describe(`Massive detection function`, function () {
  it(`Should return null if URL not passed`, () => {
    expect(detect()).equal(null);
  });

  it(`Should detect as CodePen "http://codepen.io/FWeinb/pen/wjzyH"`, () => {
    expect(detect('http://codepen.io/FWeinb/pen/wjzyH')).not.equal(null);
    expect(detect('http://codepen.io/FWeinb/pen/wjzyH').provider).equal('CodePen');
  });

  it(`Should detect as YouTube "https://www.youtube.com/watch?v=Z9irdrSZ9Ys"`, () => {
    expect(detect('https://www.youtube.com/watch?v=Z9irdrSZ9Ys')).not.equal(null);
    expect(detect('https://www.youtube.com/watch?v=Z9irdrSZ9Ys').provider).equal('YouTube');
  });

});
